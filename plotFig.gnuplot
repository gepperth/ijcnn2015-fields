#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 4.4 patchlevel 3
#    	last modified March 2011
#    	System: Linux 3.0.0-16-generic
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2010
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help seeking-assistance"
#    	immediate help:   type "help"
#    	plot window:      hit 'h'
# set terminal wxt 0
# set output
unset clip points
set clip one
unset clip two
set bar 1.000000 front
set border 31 front linetype -1 linewidth 1.000
set xdata
set ydata
set zdata
set x2data
set y2data
set timefmt x "%d/%m/%y,%H:%M"
set timefmt y "%d/%m/%y,%H:%M"
set timefmt z "%d/%m/%y,%H:%M"
set timefmt x2 "%d/%m/%y,%H:%M"
set timefmt y2 "%d/%m/%y,%H:%M"
set timefmt cb "%d/%m/%y,%H:%M"
set boxwidth
set style fill  empty border
set style rectangle back fc lt -3 fillstyle   solid 1.00 border lt -1
set style circle radius graph 0.02, first 0, 0 
set dummy x,y
set format x "% g"
set format y "% g"
set format x2 "% g"
set format y2 "% g"
set format z "% g"
set format cb "% g"
set angles radians
unset grid
set key title ""
set key inside right top vertical Right noreverse enhanced autotitles nobox
set key noinvert samplen 4 spacing 1 width 0 height 0 
set key maxcolumns 0 maxrows 0
unset key
unset label
unset arrow
set style increment default
unset style line
unset style arrow
set style histogram clustered gap 2 title  offset character 0, 0, 0
unset logscale
set offsets 0, 0, 0, 0
set pointsize 1
set pointintervalbox 1
set encoding default
unset polar
unset parametric
unset decimalsign
#set view map
set samples 10000, 10000
set isosamples 100, 100
set surface
unset contour
set clabel '%8.3g'
set mapping cartesian
set datafile separator whitespace
set hidden3d back offset 1 trianglepattern 3 undefined 1 altdiagonal bentover
set cntrparam order 4
set cntrparam linear
set cntrparam levels auto 5
set cntrparam points 5
set size ratio 0 1,1
set origin 0,0
set style data points
set style function lines
set xzeroaxis linetype -2 linewidth 1.000
set yzeroaxis linetype -2 linewidth 1.000
set zzeroaxis linetype -2 linewidth 1.000
set x2zeroaxis linetype -2 linewidth 1.000
set y2zeroaxis linetype -2 linewidth 1.000
set ticslevel 0.5
set mxtics default
set mytics default
set mztics default
set mx2tics default
set my2tics default
set mcbtics default
set noxtics
set noytics
set ztics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0
set ztics autofreq  norangelimit
set nox2tics
set noy2tics
set cbtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0
set cbtics autofreq  norangelimit
set title "" 
set title  offset character 0, 0, 0 font "" norotate
set timestamp bottom 
set timestamp "" 
set timestamp  offset character 0, 0, 0 font "" norotate
set rrange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set trange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set urange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set vrange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set xlabel "" 
set xlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set x2label "" 
set x2label  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set xrange [ * : * ] noreverse nowriteback  # (currently [-100.000:100.000] )
set x2range [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set ylabel "" 
set ylabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set y2label "" 
set y2label  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set yrange [ * : * ] noreverse nowriteback  # (currently [100.000:-100.000] )
set y2range [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set zlabel "" 
set zlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set zrange [ * : * ] noreverse nowriteback  # (currently [0.00000:1.00000] )
set cblabel "" 
set cblabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set cbrange [ * : * ] noreverse nowriteback  # (currently [0.00000:1.00000] )
set zero 1e-08
set lmargin  -1
set bmargin  -1
set rmargin  -1
set tmargin  -1
#set locale "en_US.UTF-8"
set pm3d implicit at s
set pm3d scansautomatic
set pm3d interpolate 1,1 flush begin noftriangles nohidden3d corners2color mean
set palette positive nops_allcF maxcolors 0 gamma 1.5 color model RGB 
set palette rgbformulae 7, 5, 15
set colorbox vertical origin screen 0.9, 0.2, 0 size screen 0.05, 0.6, 0 front bdefault
unset colorbox
set loadpath 
set fontpath 
set fit noerrorvariables

set term png
set view map
set title "input to layer" font "Arial,20"
set output "figs/fig11.png"
splot [-100:100] [-100:100] exp(-(x*x+y*y)/(2*20))+ 0.5*exp(-((x-30)**2+(y-30)**2)/(2*20))+0.6*exp(-((x+50)**2+(y-60)**2)/(2*20))

set output "figs/fig12.png"
set title "layer activity" font "Arial,20"
splot [-100:100] [-100:100] exp(-(x*x+y*y)/(2*20))+ 1*exp(-((x-30)**2+(y-30)**2)/(2*20))+0*exp(-((x+50)**2+(y-60)**2)/(2*20))


set output "figs/figState.png"
set title "initial layer activity" font "Arial,20"
splot [-100:100] [-100:100] (0.5*exp(-(x*x+y*y)/(2*20)))+ 0.1*(exp(-((x-30)**2+(y-30)**2)/(2*20)))+0.07*(exp(-((x+50)**2+(y-60)**2)/(2*20)))

set output "figs/figLat.png"
set title "lateral input" font "Arial,20"
splot [-100:100] [-100:100] 0.8*sqrt(exp(-(x*x+y*y)/(2*20)))+ 0.5*sqrt(exp(-((x-30)**2+(y-30)**2)/(2*20)))+0.3*sqrt(exp(-((x+50)**2+(y-60)**2)/(2*20)))


set view map
set output "figs/figTrain1.png"
set title "train pattern 1" font "Arial,40"
splot [0:100] [0:100] (exp(-((x-30)**2+(y-50)**2)/(2*20)))

set output "figs/figTrain2.png"
set title "train pattern 2" font "Arial,40"
splot [0:100] [0:100] (exp(-((x-50)**2+(y-50)**2)/(2*20)))

set output "figs/figTrain3.png"
set title "train pattern 3" font "Arial,40"
splot [0:100] [0:100] (exp(-((x-10)**2+(y-50)**2)/(2*20))) + (exp(-((x-90)**2+(y-50)**2)/(2*20)))

set output "figs/figTestSchema.png"
set label "A" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,75 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,75 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,75 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,75 textcolor rgb "#FFFFFF"
set title "test pattern locations" font "Arial,40"
unset title
splot [0:100] [0:100] (exp(-((x-10)**2+(y-50)**2)/(2*20))) + (exp(-((x-30)**2+(y-50)**2)/(2*20)))  + (exp(-((x-50)**2+(y-50)**2)/(2*20)))  + (exp(-((x-70)**2+(y-50)**2)/(2*20)))  + (exp(-((x-90)**2+(y-50)**2)/(2*20)))
unset label


set output "figs/stim1.png"
unset label
set label "A" front font "Arial,50" at 5,5 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,5 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,5 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,5 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,5 textcolor rgb "#FFFFFF"
set label "Stim.1" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
unset title
splot [0:100] [0:100] (exp(-((x-70)**2+(y-50)**2)/(2*20)))

set output "figs/stim2.png"
unset label
set label "A" front font "Arial,50" at 5,5 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,5 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,5 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,5 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,5 textcolor rgb "#FFFFFF"
set label "Stim.2" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
unset title
splot [0:100] [0:100] (exp(-((x-30)**2+(y-50)**2)/(2*20)))

set output "figs/stim3.png"
unset label
set label "A" front font "Arial,50" at 5,5 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,5 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,5 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,5 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,5 textcolor rgb "#FFFFFF"
set label "Stim.3" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
unset title
splot [0:100] [0:100] (exp(-((x-30)**2+(y-50)**2)/(2*20))) + (exp(-((x-50)**2+(y-50)**2)/(2*20)))

set output "figs/stim4.png"
unset label
set label "A" front font "Arial,50" at 5,5 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,5 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,5 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,5 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,5 textcolor rgb "#FFFFFF"
set label "Stim.4" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
unset title
splot [0:100] [0:100] 0.9*(exp(-((x-30)**2+(y-50)**2)/(2*20))) + (exp(-((x-50)**2+(y-50)**2)/(2*20)))

set output "figs/stim5.png"
unset label
set label "A" front font "Arial,50" at 5,5 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,5 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,5 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,5 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,5 textcolor rgb "#FFFFFF"
set label "Stim.5" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
unset title
splot [0:100] [0:100] (exp(-((x-50)**2+(y-50)**2)/(2*20))) + (exp(-((x-70)**2+(y-50)**2)/(2*20)))

set output "figs/stim6.png"
unset label
set label "A" front font "Arial,50" at 5,5 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,5 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,5 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,5 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,5 textcolor rgb "#FFFFFF"
set label "Stim.6" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
unset title
splot [0:100] [0:100] (exp(-((x-50)**2+(y-50)**2)/(2*20))) + 0.9*(exp(-((x-70)**2+(y-50)**2)/(2*20)))

set output "figs/stim7.png"
unset label
set label "A" front font "Arial,50" at 5,5 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,5 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,5 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,5 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,5 textcolor rgb "#FFFFFF"
set label "Stim.7" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
unset title
splot [0:100] [0:100] (exp(-((x-10)**2+(y-50)**2)/(2*20))) + 0.8*(exp(-((x-90)**2+(y-50)**2)/(2*20)))

set output "figs/stim8.png"
unset label
set label "A" front font "Arial,50" at 5,5 textcolor rgb "#FFFFFF"
set label "B" front font "Arial,50" at 25,5 textcolor rgb "#FFFFFF"
set label "C" front font "Arial,50" at 45,5 textcolor rgb "#FFFFFF"
set label "D" front font "Arial,50" at 65,5 textcolor rgb "#FFFFFF"
set label "E" front font "Arial,50" at 85,5 textcolor rgb "#FFFFFF"
set label "Stim.8" front font "Arial,50" at 5,75 textcolor rgb "#FFFFFF"
unset title
splot [0:100] [0:100] (exp(-((x-10)**2+(y-50)**2)/(2*20))) + 0.5*(exp(-((x-90)**2+(y-50)**2)/(2*20)))









set palette model  XYZ
unset key
unset title
set xlabel "neuron index" font "Arial,15"
set ylabel "neuron index" font "Arial,15"
set xtics 0,1,9
set ytics 0,1,9
set term pdf color
set output "figs/figCorr.pdf"
plot "cov.txt" u 1:2:(($3)/0.007) matrix with image

set output "figs/figProb.pdf"
set pm3d
set isosamples 50
set colorbox
#set palette default
unset xlabel
unset ylabel
set xtics 0,0.2 font "Arial,12"
set ytics 0,0.2 font "Arial,12"
set view 45,70
splot [0:1] [0:1] 1./(2.*3.14*0.2)*exp(-((x-0.5)**2+(y-0.2)**2)/(2.*0.04))


set output "figs/figResults1.pdf"
unset colorbox
set xlabel "Iteration" font "Arial,15" offset 0,-1
set ylabel "Activity at site .." font "Arial,15" offset -3
set key noreverse width 1.2 at graph 0.18,0.8   box lw 3
set xtics 86400,1200
plot [86000:86400+8*400] "generic.log" u 1:2 w p t "A", "generic.log" u 1:3 w p t "B", "generic.log" u 1:4 w p t "C", "generic.log" u 1:5 w p t "D", "generic.log" u 1:6 w p t "E"


set output "figs/figResults2.pdf"
set xlabel "Iteration" font "Arial,15" offset 0,-1
set  ylabel "Activity" font "Arial,15" offset -3
set xtics 0,10 font "Arial,10"
set ytics 0,0.1 font "Arial,10"
unset key
set key top left box lw 3
plot [0:50] "generic.log" u ($1-87200):3 w p t "Stim.3",  "generic.log" u ($1-88000):4 w p t "Stim.5", "generic.log" u ($1-89200):2 w p t "Stim.7"


#    EOF
